package com.example.mvvmcomplex2;

import android.app.Dialog;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mvvmcomplex2.databinding.AddMainBinding;
import com.example.mvvmcomplex2.databinding.ItemMainBinding;

import java.util.List;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaAdapter.ViewHolder> {

   private List<Mahasiswa> mahasiswas;

    public MahasiswaAdapter(List<Mahasiswa> mahasiswas) {
        this.mahasiswas = mahasiswas;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemMainBinding binding = ItemMainBinding.inflate(layoutInflater,parent,false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Mahasiswa mahasiswa = mahasiswas.get(position);
        System.out.println(mahasiswa.getNim());
        System.out.println(mahasiswa.getNama());
        System.out.println(mahasiswa.getTelp());

        holder.binding.txtNama.setText(mahasiswa.getNama());
        holder.binding.txtNim.setText(mahasiswa.getNim());
        holder.binding.txtTelp.setText(String.valueOf(mahasiswa.getTelp()));
        holder.binding.btnDeleted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mahasiswas.remove(position);
                notifyDataSetChanged();
            }
        });

        holder.binding.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dialog  = new Dialog(view.getContext());

                //set binding
                AddMainBinding binding =  DataBindingUtil.inflate(LayoutInflater.from(view.getContext()), R.layout.add_main, null, false);
                dialog.setContentView(binding.getRoot());

                //set ukuran
                DisplayMetrics metrics = view.getResources().getDisplayMetrics();
                int width = metrics.widthPixels;
                int height = metrics.heightPixels;
                dialog.getWindow().setLayout((6 * width) / 7, (4 * height) / 5);
                dialog.setCancelable(true);

                //set data
                binding.setNim(mahasiswa.getNim());
                binding.setNama(mahasiswa.getNama());
                binding.setTelp(mahasiswa.getTelp());

                //set atribut
                binding.btnTambah.setText("Ubah");

                binding.btnTambah.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mahasiswas.set(position,new Mahasiswa(binding.getNim(),binding.getNama(),binding.getTelp()));
                        notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return mahasiswas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemMainBinding binding;
        public ViewHolder(ItemMainBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
