package com.example.mvvmcomplex2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.Dialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.mvvmcomplex2.databinding.ActivityMainBinding;
import com.example.mvvmcomplex2.databinding.AddMainBinding;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private MainViewModel viewModel;

    private Dialog dialog;
    private AddMainBinding dialogBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        initDialog();

        binding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });

        dialogBinding.btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewModel.tambah(dialogBinding.getNim(), dialogBinding.getNama(), dialogBinding.getTelp());
                clearAdd();
                dialog.dismiss();
            }
        });

        binding.rvMain.setItemAnimator(new DefaultItemAnimator());
        binding.rvMain.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        viewModel.getMahasiswa().observe(this, new Observer<List<Mahasiswa>>() {
            @Override
            public void onChanged(List<Mahasiswa> mahasiswas) {

                if (mahasiswas.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Tambah Mahasiswa Terlebih Dahulu", Toast.LENGTH_LONG).show();
                }
                binding.rvMain.setAdapter(new MahasiswaAdapter(mahasiswas));
            }
        });


    }

    private void clearAdd() {
        dialogBinding.setTelp("");
        dialogBinding.setNim("");
        dialogBinding.setNama("");
    }

    private void initDialog() {
        dialog = new Dialog(MainActivity.this);
        dialogBinding = DataBindingUtil.inflate(LayoutInflater.from(MainActivity.this), R.layout.add_main, null, false);
        dialog.setContentView(dialogBinding.getRoot());

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        dialog.getWindow().setLayout((6 * width) / 7, (4 * height) / 5);

        dialog.setCancelable(true);
    }


}