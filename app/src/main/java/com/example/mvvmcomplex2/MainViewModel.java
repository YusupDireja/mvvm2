package com.example.mvvmcomplex2;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends ViewModel {

    private MutableLiveData<List<Mahasiswa>> data = new MutableLiveData<>();
    private List<Mahasiswa> mahasiswas = new ArrayList<>();

    public void tambah(String nim, String nama, String telp) {
        mahasiswas.add(new Mahasiswa(nim, nama, telp));
    }

    //untuk observe data mahasiswa
    public LiveData<List<Mahasiswa>> getMahasiswa() {
        data.setValue(mahasiswas);
        return data;
    }

}
